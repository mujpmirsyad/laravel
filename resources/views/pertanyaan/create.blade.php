@extends('adminlte.master')

@section('content')
  <div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Buat Pertanyaan</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/pertanyaan" method="POST">
        @csrf
      <div class="card-body">
        <div class="form-group">
          <label for="judul">Judul</label>
          <input type="text" class="form-control" name="judul" value="{{ old('judul', '') }}" id="judul" placeholder="Judul">
          @error('judul')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>

        <div class="form-group">
          <label for="isi">Isi</label>
          <input type="text" class="form-control" name="isi" value="{{ old('isi', '') }}" id="isi" placeholder="Isi">
          @error('isi')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>

      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
  </div>
@endsection