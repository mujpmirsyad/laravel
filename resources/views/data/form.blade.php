<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up!</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <form action="/welcome" method="post">
        @csrf
        <h2>Sign Up Form</h2>
        <p>First name:</p>
        <input type="text" id="nama_depan" name="namadepan" required>
        <p>Last name:</p>
        <input type="text" id="nama_belakang" name="namabelakang" required>
        <p>Gender:</p>
        <input type="radio" name="gender" value="0">Male<br>
        <input type="radio" name="gender" value="1">Female<br>
        <input type="radio" name="gender" value="2" checked>Other<br>
        <p>Nationality:</p>
        <select name="kewarganegaraan">
            <option value="idn">Indonesian</option>
            <option value="mls">Malaysian</option>
            <option value="sgp">Singaporean</option>
            <option value="aus">Autralian</option>
        </select>
        <p>Language spoken:</p>
        <input type="checkbox" name="bhs" value="0" checked>Bahasa Indonesia<br>
        <input type="checkbox" name="bhs" value="1">English<br>
        <input type="checkbox" name="bhs" value="2">Other<br>
        <p>Bio:</p>
        <textarea cols="30" rows="10"></textarea>
        <br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>